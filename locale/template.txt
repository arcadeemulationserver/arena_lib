# version 5.0.0
# author(s):
# reviewer(s):
# textdomain: arena_lib

# api.lua
arena=
team=
spectator=
[!] You can't change type!=
Parameter @1 successfully overwritten=
[!] Parameters don't seem right!=
Arena @1 successfully created=
Arena @1 successfully removed=
Arena @1 successfully renamed in @2=
@1's author succesfully removed=
@1's author succesfully changed to @2=
Players amount successfully changed ( min @1 | max @2 )=
[!] Teams are not enabled!=
[!] Nothing to do here!=
Teams successfully enabled for the arena @1=
Teams successfully disabled for the arena @1=
[!] This team doesn't exist!=
[!] No spawner with that ID to overwrite!=
[!] No team must be specified for this function!=
Spawn point #@1 successfully overwritten=
[!] No spawner with that ID to delete!=
All the spawn points belonging to team @1 have been removed=
All the spawn points have been removed=
Spawn point #@1 successfully deleted=
[!] Unknown parameter!=
[!] There's already a spawn in this point!=
[!] Spawn points can't exceed the maximum number of players!=
Spawn point #@1 successfully set=
[!] That's not an arena_lib sign!=
Sign of arena @1 successfully removed=
[!] This sign doesn't belong to @1!=
[!] There is already a sign for this arena!=
[!] There is no sign to remove assigned to @1!=
Sign of arena @1 successfully set=
Background music of arena @1 successfully overwritten=
[!] Timers are not enabled in this mod!=
Arena @1's timer is now @2 seconds=
[!] Insufficient spawners, the arena can't be enabled!=
[!] Sign not set, the arena can't be enabled!=
Arena @1 successfully enabled=
[!] The arena is already disabled!=
[!] You can't disable an arena during an ongoing game!=
[!] The arena you were queueing for has been disabled... :(=
Arena @1 successfully disabled=
@1 wins the game=
Team @1 wins the game=
[!] This minigame doesn't exist!=
[!] This arena doesn't exist!=
[!] No ongoing game!=
Game in arena @1 successfully terminated=
@1 has been eliminated by @2=
@1 has been eliminated=
@1 has been kicked by @2=
@1 has been kicked=
The arena has been forcibly terminated by @1=
The arena has been forcibly terminated=
@1 has quit the match=
Waiting for more players...=
The queue has been cancelled due to not enough players=
There are no other teams left, you win!=
You're the last player standing: you win!=
You win the game due to not enough players=
[!] This action can't be performed with no spawners set!=
Wooosh!=
[!] An arena with that name exists already!=
[!] The name contains unsupported characters!=
[!] on_timeout callback must be declared to properly use a decreasing timer!=

# commands.lua
player=
Kicks a player from an ongoing game=
[!] This player is not online!=
[!] The player must be in a game to perform this action!=
[!] You must be in a game to perform this action!=
Player successfully kicked=
minigame=
Tweaks the minigame settings for the current server=
arena name=
Forcibly ends an ongoing game=
Quits an ongoing game=
[!] You can't perform this action if you're the only one left!=
message=
Writes a message in the arena global chat while in a game=
Writes a message in the arena team chat while in a game (if teams are enabled)=

# debug_utilities.lua
name: =
Total arenas: =
Players required per team: =
Players supported per team: =
Players inside per team: =
in queue=
loading=
in game=
celebrating=
waiting=
Initial time: =
current: =
Author: =
Name: =
Teams: =
Disabled damage types: =
Players required: =
Players supported: =
Players inside: =
Enabled: =
Status: =
Sign: =
Spawn points: =
Properties: =
Temp properties: =
Team properties: =
Player: =
, kills: =
, deaths: =

# items.lua
You're immune!=

# minigame_settings.lua
Minigame settings=
Overwrite=

# signs.lua
Arena sign=
[!] You must leave the editor first!=
[!] Only the party leader can enter the queue!=
[!] You must wait for all your party members to finish their ongoing games before entering a new one!=
[!] There is not enough space for the whole party!=
[!] There is no team with enough space for the whole party!=
[!] Only the party leader can leave the queue!=
[!] The arena is not enabled!=
[!] The arena is already full!=
[!] The arena is loading, try again in a few seconds!=
[!] This minigame doesn't allow to join while in progress!=
Waiting for more players...=
The queue has been cancelled due to not enough players=
@1 seconds for the match to start=
You've joined team @1=
Game begins in @1!=
Get ready!=

# utils.lua
[!] You must disable the arena first!=
[!] There must be no one inside the editor of the arena to perform this command! (now inside: @1)=

# dependencies/parties.lua
[!] You can't perform this action while in queue!=
[!] You can't perform this action while in game!=
[!] The party leader must not be in queue to perform this action!=
[!] The party leader must not be in game to perform this action!=

# editor/editor_main.lua
Arena_lib editor | Now editing: @1=

# editor/editor_icons.lua
Players=
Players | num to set: @1 (left/right click slot #3 to change)=
Values are PER TEAM!=
Spawners=
Spawners | sel. ID: @1 (right click slot #2 to change)=
Signs=
One sign per arena=
Settings=
Arena settings=
Info=
Go back=
Enable and leave=
Leave=

# editor/tools_bgm.lua
Set BGM=
leave empty to remove the current track=
Audio file=
Title=
Author=
Volume=
Pitch=
Apply=
[!] This audio track doesn't exist!=

# editor/tools_players.lua
Change the current number=
Teams: on (click to toggle off)=
Teams: off (click to toggle on)=

# editor/tools_settings.lua
Arena name and author=
Arena properties=
Delete arena=
Rename arena=
Change author=
Set timer=
Are you sure you want to delete arena @1?=
Cancel=
Yes=

# editor/tools_spawner.lua
Add spawner=
Remove spawner=
Add team spawner=
Remove team spawner=
Switch team=
Selected team: @1=
Delete all spawners=
Delete all spawners of a team=

# editor/tools_sign.lua
Add sign=
Remove sign=

# hud/hud_waypoints.lua
[!] Waypoints are not enabled!=

# spectate/spectate_main.lua
[!] Spectate mode not supported!=
Currently spectating: @1=

# spectate/spectate_tools.lua
Change player=
Change team=
Enter the match=
